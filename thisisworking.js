

var N = 16, // Number of capsules in a wheel
    r = 0.76, //76 centimeters
    thickness = 0.18; // Thickness of the wheel capsules

var scaleIt = 1;
r *= scaleIt;
thickness *= scaleIt;

var circumference = 4.78;
var centimetersPerMile = 160934;
var gearRatio = 1;
var throttleStart = 0;

var truckGroup = Math.pow(2, 0);
var groundGroup = Math.pow(2, 1);

var torque = 1;

var stalled = false;


//angularVelocity is radians per second
//to get rpm
var radiansPerRotation = Math.PI * 2; //once around
//var rotationsPerSecond = 
var currentGear = 4;
var gearMap = [
	-0.04,
	-0.03,
	-0.02,
	-0.01,
	0.0,
	0.01,
	0.02,
	0.03,
	0.04
];
var finalRatio = gearMap[currentGear];
console.info("finalRatio", finalRatio);

var ratioToUse = finalRatio;

var cmPerMile = 160934;
var rpm = 160934 / ((r * 100 * 2) * Math.PI);
var speedLimit = rpm / 2;
var speedLimitMotor = speedLimit / finalRatio;

//----
var cmInMile = 160934;
var wheelDiameter = 76;
//2 PI r
var wheelCircumference = 2 * Math.PI * (wheelDiameter/2);
var rotationsPerMile = cmInMile / wheelCircumference;
//rpm is per minute and this many revs in a mile so 60 minutes in an hour
var targetMPH = 30;

var rpm = rotationsPerMile * (targetMPH / 60);

//-----


circumference = Math.PI * (r * 2);
//circumference *= 100;

var rpm = 50;
var mph = (60 * rpm * circumference) / (5280*12);
//Ghetto math says 50 rpm of the tire = 22mph?

var app = new p2.WebGLRenderer(setup,
	{
		width: 500,
    		height: 500
	});


function setup() {
    document.addEventListener("keydown", keydownHandler);
    buildHUD();
    var renderer = this; 
    init(renderer);
    dat.GUI.toggleHide();
    stalled = true;
    //flyWheel.angularVelocity = (-radiansPerRotation / 60) * 600;

}

function keydownHandler(event) {
	var newGear = currentGear;
	console.info(event.keyCode);
	switch (event.keyCode) {
		case 38:
			console.info("up");
			newGear++;
			break;
		case 40:
			console.info("down");
			newGear--;
			break;
		case 83:
			console.info("starter");
			    flyWheel.angularVelocity = (-radiansPerRotation / 60) * 1500;
			stalled = false;
			break;
	}

	if (newGear > 8) {
		newGear = 8;
	}
	if (newGear < 0) {
		newGear = 0;
	}

	changeGear(newGear);
}

function changeGear(gear) {
	if (gear == currentGear) {
		return;
	}
	console.info("change gear");
	console.info(gear);

	var newRatio = gearMap[gear];
	console.info(newRatio);

	torque = 1;
	//driveShaft.setMaxTorque(torque);
	wheelBodyA.angularVelocity = flyWheel.angularVelocity * newRatio;
	wheelBodyB.angularVelocity = flyWheel.angularVelocity * newRatio;

	wheelBodyA.angularVelocity = 0;
	wheelBodyB.angularVelocity = 0;
	wheelBodyA.angularForce = 0;
	wheelBodyB.angularForce = 0;
	world.removeConstraint(driveShaft);

	ratioToUse = driveShaft.ratio;
	finalRatio = newRatio;
	driveShaft.ratio = finalRatio;
	if (newRatio == 0) {
		console.info("remove");
		world.removeConstraint(driveShaft);
		ratioToUse = 0;
		driveShaft.ratio = ratioToUse
	} else {
		if (currentGear == 4) {
			console.info("add");
			//world.addConstraint(driveShaft);
		}
	}
	currentGear = gear;

	console.info("starting from", ratioToUse);
	console.info("going to", finalRatio);
}

function buildHUD() {
	var hud = document.createElement("div");
	document.body.appendChild(hud);

	//rpm
	var rpmElement = document.createElement("div");
	var rpmLabel = document.createElement("span");
	rpmLabel.textContent = "rpm: ";
	window.rpmActive = document.createElement("span");
	rpmActive.textContent = "0";

	rpmElement.appendChild(rpmLabel);
	rpmElement.appendChild(rpmActive);

	hud.appendChild(rpmElement);
	
	//mph
	var mphElement = document.createElement("div");
	var mphLabel = document.createElement("span");
	mphLabel.textContent = "mph: ";
	window.mphActive = document.createElement("span");
	mphActive.textContent = "0";

	mphElement.appendChild(mphLabel);
	mphElement.appendChild(mphActive);

	hud.appendChild(mphElement);
}

function init(renderer) {
    var world = new p2.World({
        //gravity: [0, -10]
    });

    
	world.solver.iterations = 20;

    renderer.setWorld(world);

    createGround(world);
    var chassisBody = buildChasssis(world);

    window.flyWheel = new p2.Body({
        mass: .1,
	    position: [3, 0]
    });
    flyWheel.addShape(new p2.Circle({ radius: 0.5,
    collisionGroup: truckGroup,
    //collisionMask: groundGroup,
    }));
    world.addBody(flyWheel);
	var constraint = new p2.RevoluteConstraint(flyWheel, chassisBody, {
		localPivotA: [0, 0],
		localPivotB: [3, 0]
	});
	world.addConstraint(constraint);
    


    var wheelFunc = createWheel;
    window.wheelBodyA = wheelFunc(world, [-2.3 * r * 1.5, 0], wheelMaterial);
    window.wheelBodyB = wheelFunc(world, [2.3 * r * 1.5, 0], wheelMaterial);

    var c1 = new p2.PrismaticConstraint(chassisBody,wheelBodyA,{
        localAnchorA : [
            wheelBodyA.position[0] - chassisBody.position[0],
            wheelBodyA.position[1] - chassisBody.position[1]
        ],
        localAnchorB : [0,0],
        localAxisA : [0,1],
        disableRotationalLock : true,
        collideConnected: false
    });
    var c2 = new p2.PrismaticConstraint(chassisBody,wheelBodyB,{
        localAnchorA : [
            wheelBodyB.position[0] - chassisBody.position[0],
            wheelBodyB.position[1] - chassisBody.position[1]
        ],
        localAnchorB : [0,0],
        localAxisA : [0,1],
        disableRotationalLock : true,
        collideConnected: false
    });
    //c1.setLimits(-0.5, 0.4); // Don't let the wheels move too much
    //c2.setLimits(-0.5, 0.4);
    c1.setLimits(-0.5, 0.5); // Don't let the wheels move too much
    c2.setLimits(-0.5, 0.5); // Don't let the wheels move too much
    world.addConstraint(c1);
    world.addConstraint(c2);

    var springA = new p2.DistanceConstraint(wheelBodyA, chassisBody, {
        //maxForce: 400
        maxForce: 200
    });
    world.addConstraint(springA);

    var springB = new p2.DistanceConstraint(wheelBodyB, chassisBody, {
        //maxForce: 400
        maxForce: 200
    });
    world.addConstraint(springB);

    // Add circle bumps along the ground
    for(var i=0; i<20; i++){
    circleBody = new p2.Body({
        position:[6+6*i + Math.random()*5, -2] // Set initial position
    });
    circleBody.addShape(new p2.Circle({ radius: 1 + (Math.random() * 3) * scaleIt, material: groundMaterial,
	    collisionGroup: groundGroup,
	    collisionMask: truckGroup,
    }));
    //circleBody.addShape(new p2.Circle({ radius: 2*Math.random(), material: groundMaterial }));
    world.addBody(circleBody);
    }



	window.driveShaft = new p2.GearConstraint(flyWheel, wheelBodyA, {
		ratio: finalRatio,
		world
	});
	if (finalRatio != 0) {
		console.info("added constraint");
		//world.addConstraint(driveShaft);
	}

	var constraintB = new p2.GearConstraint(wheelBodyA, wheelBodyB, {
		ratio: 1,
		//maxTorque: 1000
		world
	});
	world.addConstraint(constraintB);


    hookupControls(renderer, wheelBodyA, wheelBodyB);

    renderer.followBody = chassisBody;
    renderer.frame(0, 0, 20, 20);
}

function createWheel2(world, position, material){

    var wheelBody = new p2.Body({
        mass: 1,
        position: position
    });
    wheelBody.addShape(new p2.Circle({ radius: r, material: material,
    collisionGroup: truckGroup,
    collisionMask: groundGroup,
    }));
    world.addBody(wheelBody);

    return wheelBody;
}

// Creates a soft wheel in the given world at the given position.
// Returns the center body.
function createWheel(world, position, material){

  // Create the center circle body
  var wheelBody = new p2.Body({
    mass: 2,
    position: position
  });
  //wheelBody.addShape(new p2.Circle({ radius: r/3 }));
  wheelBody.addShape(new p2.Circle({
	collisionGroup: truckGroup,
	collisionMask: groundGroup,
	  radius: r/4, material: material }));
  world.addBody(wheelBody);

  // Create a chain of capsules around the center.
  var lastBody, firstBody;
  var linkLength = Math.sqrt(r*r + r*r - 2*r*r*Math.cos(2 * Math.PI / N));
  for(var i=0; i<N; i++){

    // Create a capsule body
    var angle = i / N * Math.PI * 2;
    var x = r * Math.cos(angle) + position[0];
    var y = r * Math.sin(angle) + position[1];
    var body = new p2.Body({
      mass: 0.1,
      position: [x,y],
        angle: angle + (Math.PI / 2),
    });
    body.addShape(new p2.Capsule({
	collisionGroup: truckGroup,
	collisionMask: groundGroup,
	    radius: thickness/2, length: linkLength, material: material }));
    world.addBody(body);

    // Constrain the capsule body to the center body.
    // A prismatic constraint lets it move radially from the center body along one axis
    if (i % 1 == 0) {
	    var prismatic = new p2.PrismaticConstraint(wheelBody, body, {
	      localAnchorA :  [0, 0],
	      localAnchorB :  [0, 0],
	      localAxisA :    [Math.cos(angle), Math.sin(angle)],
	      disableRotationalLock: true, // Let the capsule rotate around its own axis
	      collideConnected: false,
		upperLimit: 1
	    });
	    world.addConstraint(prismatic);
    }

    // Make a "spring" that keeps the body from the center body at a given distance with some flexing
    world.addConstraint(new p2.DistanceConstraint(wheelBody, body, {
      maxForce: 100 // Allow flexing
    }));

    if(lastBody){
      // Constrain the capsule to the previous one.
      var c = new p2.RevoluteConstraint(body, lastBody, {
        localPivotA: [-linkLength/2, 0],
        localPivotB: [linkLength/2, 0],
        collideConnected: false
      });
      world.addConstraint(c);
    } else {
      firstBody = body;
    }

    lastBody = body;
  }

  // Close the capsule circle
  /*
  world.addConstraint(new p2.RevoluteConstraint(firstBody, lastBody, {
    localPivotA: [-linkLength/2, 0],
    localPivotB: [linkLength/2, 0],
    collideConnected: false
  }));
  */


  return wheelBody;
}

function buildChasssis(world) {
  //TODO reformat this
  // Create chassis
  var temp = r;
  r = temp * 1.5;
  //var chassisBody = new p2.Body({ mass : 6, position:[-0.3 * r, 0.8 * r] });
  var chassisBody = new p2.Body({ mass : 6, position:[-0.3 * r, 1.2 * r] });
  chassisBody.addShape(new p2.Capsule({ // Capsule below the chassis
    collisionGroup: truckGroup,
    collisionMask: groundGroup,
    length: 1.8 * r,
	  material: truckMaterial,
    radius: r * 0.3
  }), [r * 0.5,-r * 0.4], -0.1);
  chassisBody.addShape(new p2.Capsule({ // First capsule above the trunk
    collisionGroup: truckGroup,
    collisionMask: groundGroup,
    length: 1.8 * r,
    material: truckMaterial,
    radius: r * 0.1
  }), [-r*0.4,r*0.6], Math.PI/2);
  chassisBody.addShape(new p2.Capsule({ // Second capsule above the trunk
    collisionGroup: truckGroup,
    collisionMask: groundGroup,
    length: 1.8 * r,
    material: truckMaterial,
    radius: r * 0.1
  }), [-r*0.2,r*0.6], Math.PI/2);
  chassisBody.addShape(new p2.Capsule({ // Inclined capsule above the trunk
    collisionGroup: truckGroup,
    collisionMask: groundGroup,
    length: 1.8 * r,
    material: truckMaterial,
    radius: r * 0.1
  }), [-r*1.4,r*1], Math.PI/7);
  chassisBody.addShape(new p2.Convex({ // Main chassis shape
    collisionGroup: truckGroup,
    material: truckMaterial,
    collisionMask: groundGroup,
    vertices: [
      [3.5*r, -0.6*r],
      [3.7*r, -0.4*r],
      [3.6*r, 0.5*r],
      [3.3*r, 0.6*r],
      [-3.5*r, 0.6*r],
      [-3.55*r, -0.1*r],
      [-3.4*r, -0.6*r]
    ],
    width: 3.5*r*2,
    height: 0.6 * r * 2
  }));
  chassisBody.addShape(new p2.Convex({ // Top "window"
    collisionGroup: truckGroup,
    material: truckMaterial,
    collisionMask: groundGroup,
    vertices: [
      [r, -0.5 * r],
      [0.3 * r, 0.5 * r],
      [-r, 0.5 * r],
      [-r * 1.1, -0.5 * r]
    ]
  }), [r,0.55 * r * 2], 0);


  world.addBody(chassisBody);

  r = temp;
  return chassisBody;
}

function createGround(world) {
    window.groundMaterial = new p2.Material();
    //TODO move this
    window.wheelMaterial = new p2.Material();
    window.truckMaterial = new p2.Material();

    var groundWheelContactMaterial = new p2.ContactMaterial(groundMaterial, wheelMaterial, {
    friction: 20
    });

    window.truckContactMaterial = new p2.ContactMaterial(groundMaterial, truckMaterial, {
    friction: 0.5
    });

    world.addContactMaterial(groundWheelContactMaterial);
    world.addContactMaterial(truckContactMaterial);
    groundBody = new p2.Body({
        position: [0, -r * 2]
    });
    groundBody.addShape(new p2.Plane({
	    material: groundMaterial,
	    collisionGroup: groundGroup,
	    collisionMask: truckGroup,
    }));
    world.addBody(groundBody);

    return groundBody;
}

function hookupControls(renderer, wheelBodyA, wheelBodyB) {
  // Apply current engine torque after each step
  var left=0, right=0;
  world.on("postStep",function(evt){
    var now = new Date().getTime();
    var timeDelta = now - throttleStart;
    var extraForce = timeDelta / 200;
    extraForce = Math.min(extraForce, 4);

    flyWheel = flyWheel;

    //


    //var rpsA = Math.abs(flyWheel.angularVelocity) / radiansPerRotation;
    var rpsA = -(flyWheel.angularVelocity) / radiansPerRotation;
    var rpmA = rpsA * 60;
    rpmActive.textContent = rpmA.toFixed(0);


    var rpsWheel = Math.abs(wheelBodyA.angularVelocity) / radiansPerRotation;
    var rpmWheel = rpsWheel * 60;

    var centimetersPerMinute = rpmWheel * (r * 100 * 2);
    var cmPerHour = centimetersPerMinute * 60;
    var mph = cmPerHour / cmPerMile;
    mphActive.textContent = mph.toFixed(0);


    if (torque < 500 & finalRatio != 0) {
	torque += 10;
	world.removeConstraint(driveShaft);
	window.driveShaft = new p2.GearConstraint(flyWheel, wheelBodyA, {
		ratio: finalRatio,
		world
	});
	world.addConstraint(driveShaft);
    } 

    if (rpmA < 300) {
	    stalled = true;
	    return;
    }

    var baseForce = 20;
    flyWheel.angularForce += (left - right) * (baseForce + extraForce);
    //wheelBodyB.angularForce += (left - right) * (34 + extraForce);
    //console.info(wheelBodyB.angularForce);

    //console.info(mph);
    //console.info(mph);
    if (rpmA > 5500 && !window.yup) {
        //console.info(rpmWheel);
        //console.info(new Date().getTime() - throttleStart);
        //window.yup = true;
        flyWheel.angularForce = 0;
    }
    //idle
    if ((left - right) == 0) {
	
        flyWheel.angularForce = 4;
    }
    var idleRpm = 600;
    if (rpmA < idleRpm && (left - right) == 0) {
	
        flyWheel.angularForce += -5;
    }
  });
  renderer.on("keydown",function(evt){

    switch(evt.keyCode){
      case 39:
        right = 1;
        if (throttleStart === 0) {
            throttleStart = new Date().getTime();
        }
        break;
      case 37:
        //left = 1;
        if (throttleStart === 0) {
            throttleStart = new Date().getTime();
        }
        break;
      case 32:
	//world.addConstraint(driveShaft);
	break;
    }
  }).on("keyup",function(evt){
    switch(evt.keyCode){
      case 39:
        right = 0;
        throttleStart = 0;
        break;
      case 37:
        left = 0;
        throttleStart = 0;
        break;
    }
  });
}

function updateSpeed(evt) {
    //wheelBodyA.angularForce += (left - right) * 30;
    //wheelBodyB.angularForce += (left - right) * 30;
    //console.info(flyWheel.angularVelocity);
    var now = new Date().getTime();
    var timeDelta = now - throttleStart;
    var extraForce = timeDelta / 500;
    extraForce = Math.min(extraForce, 2);

    var baseForce = 10;
    var flyWheelForceA = (left - right) * (baseForce + (Math.abs(wheelBodyA.angularVelocity) * 3));
    var flyWheelForceB = (left - right) * (baseForce + (Math.abs(wheelBodyB.angularVelocity) * 3));

    if (left !== 0) {
        flyWheelForceA += extraForce;
        flyWheelForceB += extraForce;
    }
    if (right !== 0) {
        flyWheelForceA -= extraForce;
        flyWheelForceB -= extraForce;
    }
    //console.log(flyWheelForceB);
    
    flyWheelForceA *= gearRatio;
    flyWheelForceB *= gearRatio;

    //flyWheel.angularForce += flyWheelForce;
    wheelBodyA.angularForce += flyWheelForceA;
    wheelBodyB.angularForce += flyWheelForceB;
    //console.info(flyWheelForceA);

    var maxSpeed = 15 / gearRatio;
    //maxSpeed = 10;
    
    //console.log(wheelBodyA.angularForce);

    if ((right !== 0 && wheelBodyA.angularVelocity < -maxSpeed) ||
        (left !== 0 && wheelBodyA.angularVelocity > maxSpeed)) {
        wheelBodyA.angularForce = 0;
    }

    if ((right !== 0 && wheelBodyB.angularVelocity < -maxSpeed) ||
        (left !== 0 && wheelBodyB.angularVelocity > maxSpeed)) {
        wheelBodyB.angularForce = 0;
    }

        var rotationsPerSecond = wheelBodyA.angularVelocity / radiansPerRotation;
        var rotationsPerMinute = rotationsPerSecond * 60;
        //console.info(rotationsPerMinute);

}
