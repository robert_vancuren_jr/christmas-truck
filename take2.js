var N = 16, // Number of capsules in a wheel
    r = 0.76, //76 centimeters
    thickness = 0.18; // Thickness of the wheel capsules
var circumference = 4.78;
var centimetersPerMile = 160934;
var gearRatio = 1;
var throttleStart = 0;

var truckGroup = Math.pow(2, 0);
var groundGroup = Math.pow(2, 1);

//angularVelocity is radians per second
//to get rpm
var radiansPerRotation = Math.PI * 2; //once around
console.info(radiansPerRotation);
//var rotationsPerSecond = 
var finalRatio = 0.02;

var cmPerMile = 160934;
var rpm = 160934 / ((r * 100 * 2) * Math.PI);
var speedLimit = rpm / 2;
var speedLimitMotor = speedLimit / finalRatio;
console.info("rpm", rpm / 2); //tire rpm at 30mph

//----
console.info("yo");
var cmInMile = 160934;
var wheelDiameter = 76;
//2 PI r
var wheelCircumference = 2 * Math.PI * (wheelDiameter/2);
console.info(wheelCircumference);
var rotationsPerMile = cmInMile / wheelCircumference;
console.info("perMile", rotationsPerMile);
//rpm is per minute and this many revs in a mile so 60 minutes in an hour
var targetMPH = 30;

var rpm = rotationsPerMile * (targetMPH / 60);
console.log("60", rpm);

//-----


circumference = Math.PI * (r * 2);
//circumference *= 100;
console.info(circumference);

var rpm = 50;
var mph = (60 * rpm * circumference) / (5280*12);
console.info(mph * 100);
//Ghetto math says 50 rpm of the tire = 22mph?

var app = new p2.WebGLRenderer(setup);


function setup() {
    var renderer = this; 
    init(renderer);
}

function init(renderer) {
    var world = new p2.World({
        //gravity: [0, -10]
    });

    
	world.solver.iterations = 20;

    renderer.setWorld(world);

    createGround(world);
    var chassisBody = buildChasssis(world);

    window.flyWheel = new p2.Body({
        mass: .5,
    });
    flyWheel.addShape(new p2.Circle({ radius: 0.5,
    collisionGroup: truckGroup,
    //collisionMask: groundGroup,
    }));
    world.addBody(flyWheel);
	var constraint = new p2.RevoluteConstraint(flyWheel, chassisBody, {
		worldPivot: [0, 0]
	});
	world.addConstraint(constraint);
    


    var wheelFunc = createWheel;
    var wheelBodyA = wheelFunc(world, [-2.3 * r * 1.5, 0], wheelMaterial);
    var wheelBodyB = wheelFunc(world, [2.3 * r * 1.5, 0], wheelMaterial);

    var c1 = new p2.PrismaticConstraint(chassisBody,wheelBodyA,{
        localAnchorA : [
            wheelBodyA.position[0] - chassisBody.position[0],
            wheelBodyA.position[1] - chassisBody.position[1]
        ],
        localAnchorB : [0,0],
        localAxisA : [0,1],
        disableRotationalLock : true,
        collideConnected: false
    });
    var c2 = new p2.PrismaticConstraint(chassisBody,wheelBodyB,{
        localAnchorA : [
            wheelBodyB.position[0] - chassisBody.position[0],
            wheelBodyB.position[1] - chassisBody.position[1]
        ],
        localAnchorB : [0,0],
        localAxisA : [0,1],
        disableRotationalLock : true,
        collideConnected: false
    });
    //c1.setLimits(-0.5, 0.4); // Don't let the wheels move too much
    //c2.setLimits(-0.5, 0.4);
    c1.setLimits(-0.5, 0.5); // Don't let the wheels move too much
    c2.setLimits(-0.5, 0.5); // Don't let the wheels move too much
    world.addConstraint(c1);
    world.addConstraint(c2);

    var springA = new p2.DistanceConstraint(wheelBodyA, chassisBody, {
        //maxForce: 400
        maxForce: 200
    });
    world.addConstraint(springA);

    var springB = new p2.DistanceConstraint(wheelBodyB, chassisBody, {
        //maxForce: 400
        maxForce: 200
    });
    world.addConstraint(springB);

    // Add circle bumps along the ground
    for(var i=0; i<10; i++){
    circleBody = new p2.Body({
        position:[6+6*i + Math.random()*3, -2.8] // Set initial position
    });
    circleBody.addShape(new p2.Circle({ radius: 2, material: groundMaterial,
	    collisionGroup: groundGroup,
	    collisionMask: truckGroup,
    }));
    //circleBody.addShape(new p2.Circle({ radius: 2*Math.random(), material: groundMaterial }));
    world.addBody(circleBody);
    }


	var constraint = new p2.GearConstraint(flyWheel, wheelBodyA, {
		ratio: finalRatio,
		//maxTorque: 1000
	});
	world.addConstraint(constraint);

    flyWheel.angularVelocity = (600 / radiansPerRotation) / 60;

    hookupControls(renderer, wheelBodyA, wheelBodyB);

    renderer.followBody = chassisBody;
    renderer.frame(0, 0, 30, 30);
}

function createWheel2(world, position, material){

    var wheelBody = new p2.Body({
        mass: 1,
        position: position
    });
    wheelBody.addShape(new p2.Circle({ radius: r, material: material,
    collisionGroup: truckGroup,
    collisionMask: groundGroup,
    }));
    world.addBody(wheelBody);

    return wheelBody;
}

// Creates a soft wheel in the given world at the given position.
// Returns the center body.
function createWheel(world, position, material){

  // Create the center circle body
  var wheelBody = new p2.Body({
    mass: 2,
    position: position
  });
  //wheelBody.addShape(new p2.Circle({ radius: r/3 }));
  wheelBody.addShape(new p2.Circle({
	collisionGroup: truckGroup,
	collisionMask: groundGroup,
	  radius: r/4, material: material }));
  world.addBody(wheelBody);

  // Create a chain of capsules around the center.
  var lastBody, firstBody;
  var linkLength = Math.sqrt(r*r + r*r - 2*r*r*Math.cos(2 * Math.PI / N));
  for(var i=0; i<N; i++){

    // Create a capsule body
    var angle = i / N * Math.PI * 2;
    var x = r * Math.cos(angle) + position[0];
    var y = r * Math.sin(angle) + position[1];
    var body = new p2.Body({
      mass: 0.2,
      position: [x,y],
        angle: angle + (Math.PI / 2),
    });
    body.addShape(new p2.Capsule({
	collisionGroup: truckGroup,
	collisionMask: groundGroup,
	    radius: thickness/2, length: linkLength, material: material }));
    world.addBody(body);

    // Constrain the capsule body to the center body.
    // A prismatic constraint lets it move radially from the center body along one axis
    if (i % 1 == 0) {
	    var prismatic = new p2.PrismaticConstraint(wheelBody, body, {
	      localAnchorA :  [0, 0],
	      localAnchorB :  [0, 0],
	      localAxisA :    [Math.cos(angle), Math.sin(angle)],
	      disableRotationalLock: true, // Let the capsule rotate around its own axis
	      collideConnected: false,
		upperLimit: 1
	    });
	    world.addConstraint(prismatic);
	    console.info(i);
    }

    // Make a "spring" that keeps the body from the center body at a given distance with some flexing
    world.addConstraint(new p2.DistanceConstraint(wheelBody, body, {
      maxForce: 100 // Allow flexing
    }));

    if(lastBody){
      // Constrain the capsule to the previous one.
      var c = new p2.RevoluteConstraint(body, lastBody, {
        localPivotA: [-linkLength/2, 0],
        localPivotB: [linkLength/2, 0],
        collideConnected: false
      });
      world.addConstraint(c);
    } else {
      firstBody = body;
    }

    lastBody = body;
  }

  // Close the capsule circle
  world.addConstraint(new p2.RevoluteConstraint(firstBody, lastBody, {
    localPivotA: [-linkLength/2, 0],
    localPivotB: [linkLength/2, 0],
    collideConnected: false
  }));


  return wheelBody;
}

function buildChasssis(world) {
  //TODO reformat this
  // Create chassis
  var temp = r;
  r = temp * 1.5;
  //var chassisBody = new p2.Body({ mass : 6, position:[-0.3 * r, 0.8 * r] });
  var chassisBody = new p2.Body({ mass : 6, position:[-0.3 * r, 1.2 * r] });
  chassisBody.addShape(new p2.Capsule({ // Capsule below the chassis
    collisionGroup: truckGroup,
    collisionMask: groundGroup,
    length: 1.8 * r,
    radius: r * 0.3
  }), [r * 0.5,-r * 0.4], -0.1);
  chassisBody.addShape(new p2.Capsule({ // First capsule above the trunk
    collisionGroup: truckGroup,
    collisionMask: groundGroup,
    length: 1.8 * r,
    radius: r * 0.1
  }), [-r*0.4,r*0.6], Math.PI/2);
  chassisBody.addShape(new p2.Capsule({ // Second capsule above the trunk
    collisionGroup: truckGroup,
    collisionMask: groundGroup,
    length: 1.8 * r,
    radius: r * 0.1
  }), [-r*0.2,r*0.6], Math.PI/2);
  chassisBody.addShape(new p2.Capsule({ // Inclined capsule above the trunk
    collisionGroup: truckGroup,
    collisionMask: groundGroup,
    length: 1.8 * r,
    radius: r * 0.1
  }), [-r*1.4,r*1], Math.PI/7);
  chassisBody.addShape(new p2.Convex({ // Main chassis shape
    collisionGroup: truckGroup,
    collisionMask: groundGroup,
    vertices: [
      [3.5*r, -0.6*r],
      [3.7*r, -0.4*r],
      [3.6*r, 0.5*r],
      [3.3*r, 0.6*r],
      [-3.5*r, 0.6*r],
      [-3.55*r, -0.1*r],
      [-3.4*r, -0.6*r]
    ],
    width: 3.5*r*2,
    height: 0.6 * r * 2
  }));
  chassisBody.addShape(new p2.Convex({ // Top "window"
    collisionGroup: truckGroup,
    collisionMask: groundGroup,
    vertices: [
      [r, -0.5 * r],
      [0.3 * r, 0.5 * r],
      [-r, 0.5 * r],
      [-r * 1.1, -0.5 * r]
    ]
  }), [r,0.55 * r * 2], 0);


  world.addBody(chassisBody);

  r = temp;
  return chassisBody;
}

function createGround(world) {
    window.groundMaterial = new p2.Material();
    //TODO move this
    window.wheelMaterial = new p2.Material();

    var groundWheelContactMaterial = new p2.ContactMaterial(groundMaterial, wheelMaterial, {
    friction: 50
    });

    world.addContactMaterial(groundWheelContactMaterial);
    groundBody = new p2.Body({
        position: [0, -r * 2]
    });
    groundBody.addShape(new p2.Plane({
	    material: groundMaterial,
	    collisionGroup: groundGroup,
	    collisionMask: truckGroup,
    }));
    world.addBody(groundBody);

    return groundBody;
}

function hookupControls(renderer, wheelBodyA, wheelBodyB) {
  // Apply current engine torque after each step
  var left=0, right=0;
  world.on("postStep",function(evt){
    var now = new Date().getTime();
    var timeDelta = now - throttleStart;
    var extraForce = timeDelta / 200;
    extraForce = Math.min(extraForce, 4);

    flyWheel = flyWheel;

    flyWheel.angularForce += (left - right) * (50 + extraForce);
    //wheelBodyB.angularForce += (left - right) * (34 + extraForce);
    //console.info(wheelBodyB.angularForce);
    //
    //idle
    if (left - right == 0) {
	    //console.info("engine break");
	    //flyWheel.angularForce = -0.2;
    }


    var rpsA = Math.abs(flyWheel.angularVelocity) / radiansPerRotation;
    var rpmA = rpsA * 60;


    var rpsWheel = Math.abs(wheelBodyA.angularVelocity) / radiansPerRotation;
    var rpmWheel = rpsWheel * 60;

    var centimetersPerMinute = rpmWheel * (r * 100 * 2);
    var cmPerHour = centimetersPerMinute * 60;
    var mph = cmPerHour / cmPerMile;


    //console.info(mph);
    //console.info(mph);
    if (rpmA > 5500 && !window.yup) {
        //console.info(rpmWheel);
        //console.info(new Date().getTime() - throttleStart);
        //window.yup = true;
        flyWheel.angularForce = 0;
    }
    if (rpmWheel > 134 && !window.yup) {
        //wheelBodyB.angularForce = 0;
    }
  });
  renderer.on("keydown",function(evt){
    switch(evt.keyCode){
      case 39:
        right = 1;
        if (throttleStart === 0) {
            throttleStart = new Date().getTime();
        }
        break;
      case 37:
        left = 1;
        if (throttleStart === 0) {
            throttleStart = new Date().getTime();
        }
        break;
    }
  }).on("keyup",function(evt){
    switch(evt.keyCode){
      case 39:
        right = 0;
        throttleStart = 0;
        break;
      case 37:
        left = 0;
        throttleStart = 0;
        break;
    }
  });
}

function updateSpeed(evt) {
    //wheelBodyA.angularForce += (left - right) * 30;
    //wheelBodyB.angularForce += (left - right) * 30;
    //console.info(flyWheel.angularVelocity);
    var now = new Date().getTime();
    var timeDelta = now - throttleStart;
    var extraForce = timeDelta / 500;
    extraForce = Math.min(extraForce, 2);

    var baseForce = 10;
    var flyWheelForceA = (left - right) * (baseForce + (Math.abs(wheelBodyA.angularVelocity) * 3));
    var flyWheelForceB = (left - right) * (baseForce + (Math.abs(wheelBodyB.angularVelocity) * 3));

    if (left !== 0) {
        flyWheelForceA += extraForce;
        flyWheelForceB += extraForce;
    }
    if (right !== 0) {
        flyWheelForceA -= extraForce;
        flyWheelForceB -= extraForce;
    }
    //console.log(flyWheelForceB);
    
    flyWheelForceA *= gearRatio;
    flyWheelForceB *= gearRatio;

    //flyWheel.angularForce += flyWheelForce;
    wheelBodyA.angularForce += flyWheelForceA;
    wheelBodyB.angularForce += flyWheelForceB;
    //console.info(flyWheelForceA);

    var maxSpeed = 15 / gearRatio;
    //maxSpeed = 10;
    
    //console.log(wheelBodyA.angularForce);

    if ((right !== 0 && wheelBodyA.angularVelocity < -maxSpeed) ||
        (left !== 0 && wheelBodyA.angularVelocity > maxSpeed)) {
        wheelBodyA.angularForce = 0;
    }

    if ((right !== 0 && wheelBodyB.angularVelocity < -maxSpeed) ||
        (left !== 0 && wheelBodyB.angularVelocity > maxSpeed)) {
        wheelBodyB.angularForce = 0;
    }

        var rotationsPerSecond = wheelBodyA.angularVelocity / radiansPerRotation;
        var rotationsPerMinute = rotationsPerSecond * 60;
        //console.info(rotationsPerMinute);

}
